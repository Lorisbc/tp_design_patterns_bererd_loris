package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.IObserver;

public interface IObservable {
	
	public void attach(IObserver Observer);
    
    public void detach(IObserver Observer);
    
    public void notifyObserver() throws SensorNotActivatedException;
    

}

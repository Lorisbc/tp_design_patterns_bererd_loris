package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.IObserver;

public abstract class AbstractSensor implements ISensor {
	
	 protected double value = 0;
	 
	ArrayList<IObserver> listObserver = new ArrayList<IObserver>();

	 public void attach(IObserver Observer) {
			listObserver.add(Observer);
			
		}


		public void detach(IObserver Observer) {
			listObserver.remove(Observer);
			
		}
		
		public void notifyObserver() throws SensorNotActivatedException {
			
			for (IObserver Observer : listObserver) {
				 Observer.update();
			}
			
			
		}

	public abstract void on();

	public abstract void off();

	public abstract boolean getStatus();

	public abstract void update() throws SensorNotActivatedException;

	public abstract double getValue() throws SensorNotActivatedException;

}

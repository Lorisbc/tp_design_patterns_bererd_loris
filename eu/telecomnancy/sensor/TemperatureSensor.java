package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

import eu.telecomnancy.ui.IObserver;

public class TemperatureSensor extends AbstractSensor {
	
	
	
    boolean state;
   

    public void on() {
        state = true;
    }

    
    public void off() {
        state = false;
    }

    
    public boolean getStatus() {
        return state;
    }

   
    public void update() throws SensorNotActivatedException {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        this.notifyObserver();
    }

    
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }


   

}

package eu.telecomnancy.sensor;

import eu.telecomnancy.proxy.SensorLogger;
import eu.telecomnancy.ui.IObserver;

public class Proxy extends AbstractSensor  {
	
	private AbstractSensor sensor=new TemperatureSensor();
	private SensorLogger logger=new SensorLogger();
	

	

	public void on() {
		logger.log("on","");
		sensor.on();
	}

	public void off() {
		logger.log("off","");
		sensor.off();

	}

	public boolean getStatus() {
		boolean status=sensor.getStatus();
		logger.log("getStatus",String.valueOf(status));
		return status;
	}

	public void update() throws SensorNotActivatedException {
		logger.log("update","");
		sensor.update();
		this.notifyObserver();

	}

	public double getValue() throws SensorNotActivatedException {
		double value=sensor.getValue();
		logger.log("getValue",String.valueOf(value));
		return value;
	}

}

package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.IObserver;

public class Adapter extends AbstractSensor  {
	
	
	
	LegacyTemperatureSensor legacySensor=new LegacyTemperatureSensor();
	
	public void on() {
		if(legacySensor.getStatus()==false){
		legacySensor.onOff();
		}
		

	}

	
	public void off() {
		if(legacySensor.getStatus()==true){
			legacySensor.onOff();
			}

	}

	
	public boolean getStatus() {
		
		return legacySensor.getStatus();
	}

	
	public void update() throws SensorNotActivatedException {
		legacySensor.onOff();
		legacySensor.onOff();
		this.notifyObserver();

	}

	
	public double getValue() throws SensorNotActivatedException {
		
		return legacySensor.getTemperature();
	}

	
	


	
	

}

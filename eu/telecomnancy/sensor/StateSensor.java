package eu.telecomnancy.sensor;

import java.util.Random;

public class StateSensor extends AbstractSensor{
    
	double value = 0;
	Contexte c = new Contexte();
	
	@Override
	public void on() {
		IState on = new OnState();
		on.execute(c);
		
	}

	@Override
	public void off() {
		IState off = new OffState();
		off.execute(c);
		
	}

	@Override
	public boolean getStatus() {
		if(c.getState().getClass()==OffState.class)
			return false;
		else
			return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (this.getStatus())
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        this.notifyObserver();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		 if (this.getStatus())
	            return value;
	        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
}

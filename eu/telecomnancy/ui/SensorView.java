package eu.telecomnancy.ui;

import eu.telecomnancy.command.CommandGetValue;
import eu.telecomnancy.command.CommandOff;
import eu.telecomnancy.command.CommandOn;
import eu.telecomnancy.command.CommandUpdate;
import eu.telecomnancy.command.ICommand;
import eu.telecomnancy.command.Invoker;
import eu.telecomnancy.decorator.Conversion;
import eu.telecomnancy.decorator.Round;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel implements IObserver {
    private AbstractSensor sensor;
    private Conversion conversion;
    private Round round;
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton btn_convertisor = new JButton("°F");
    private JButton btn_round = new JButton("Round");
    
  
    
    
    private Invoker invoker;

        public SensorView(AbstractSensor c) {
            
            this.sensor = c;
            conversion=new Conversion(sensor);
            round=new Round(sensor);
            
            
            
            invoker= new Invoker();
            
            
           
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                try {
					invoker.execute(new CommandOn(sensor));
				} catch (SensorNotActivatedException e1) {
					
					e1.printStackTrace();
				}
            }
        });

        off.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
            	try {
					invoker.execute(new CommandOff(sensor));
				} catch (SensorNotActivatedException e1) {
					
					e1.printStackTrace();
				}
            }
        });

        update.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                try {
                	invoker.execute(new CommandUpdate(sensor));
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        
        btn_convertisor.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		
        		try {
					
        			if (conversion.isDegre()){
						String val=String.valueOf(conversion.convert());
						value.setText(val+" °F");
						btn_convertisor.setText("°C");
						conversion.setDegre(false);
        			}else{
        				CommandGetValue getValueCommand=new CommandGetValue(sensor);
        				invoker.execute(getValueCommand);
        				String val=String.valueOf(getValueCommand.get());
        				value.setText(val+" °C");
    					btn_convertisor.setText("°F");
    					conversion.setDegre(true);
        			}
        			
					
        		 } catch (SensorNotActivatedException sensorNotActivatedException) {
                     sensorNotActivatedException.printStackTrace();
                 }
        	}
        });
        
        btn_round.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		
        		try {
					
        			
						String val=String.valueOf(round.round());
						value.setText(val+" °C");
						
        			
					
        		 } catch (SensorNotActivatedException sensorNotActivatedException) {
                     sensorNotActivatedException.printStackTrace();
                 }
        	}
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 5));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(btn_convertisor);
        buttonsPanel.add(btn_round);

        this.add(buttonsPanel, BorderLayout.SOUTH);
        
    }

	public void update() throws SensorNotActivatedException {
		CommandGetValue getValueCommand=new CommandGetValue(sensor);
		invoker.execute(getValueCommand);
		String val=String.valueOf(getValueCommand.get());
		value.setText(val+" °C");
		
	}
}

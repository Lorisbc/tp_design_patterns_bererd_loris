package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;

import javax.swing.*;

import java.awt.*;
import java.io.IOException;

public class MainWindow extends JFrame {

    private AbstractSensor sensor;
    private SensorView sensorView;
    private Menu menu;
   

    public MainWindow(AbstractSensor sensor) {
        this.sensor = sensor;
        this.sensorView = new SensorView(this.sensor);
        this.sensor.attach(sensorView);
        
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);
        try {
			menu=new Menu(sensor);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
        this.setJMenuBar(menu);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}

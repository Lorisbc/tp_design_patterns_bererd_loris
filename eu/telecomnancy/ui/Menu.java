package eu.telecomnancy.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import eu.telecomnancy.command.CommandGetValue;
import eu.telecomnancy.command.CommandOff;
import eu.telecomnancy.command.CommandOn;
import eu.telecomnancy.command.CommandUpdate;
import eu.telecomnancy.command.ICommand;
import eu.telecomnancy.command.Invoker;
import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Menu extends JMenuBar {

	private AbstractSensor sensor;
	private JMenu menu = new JMenu();
	private JMenuItem item[];

	private Invoker invoker;


	public Menu(AbstractSensor c) throws IOException{
		this.sensor=c;

		invoker= new Invoker();

		ReadPropertyFile rp=new ReadPropertyFile();
		Properties p=rp.readFile("/eu/telecomnancy/commande.properties");
		item = new JMenuItem[p.size()];
		for (int k=0;k<p.size();k++){
			item[k]=new JMenuItem();
		}
		int k=0;
		for (String i: p.stringPropertyNames()) {

			item[k].setText(p.getProperty(i));
			item[k].addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					JMenuItem source =  (JMenuItem) e.getSource();


					try {

						//On crée un objet Class
						Constructor ct = Class.forName(source.getText()).getConstructor(AbstractSensor.class);



						ICommand c = (ICommand) ct.newInstance(sensor);

						invoker.execute(c);
					} catch (SensorNotActivatedException e1) {

						e1.printStackTrace();
					} catch (NoSuchMethodException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SecurityException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InstantiationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalArgumentException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InvocationTargetException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} 
				}
			});
			menu.add(item[k]);


			k++;
		}




		menu.setText("Commandes");
		this.add(menu);
	}




}

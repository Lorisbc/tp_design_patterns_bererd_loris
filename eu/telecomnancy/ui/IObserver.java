package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface IObserver {
	
	public void update() throws SensorNotActivatedException;

}

package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;

public class CommandOff implements ICommand {
	
	private AbstractSensor sensor;
	public CommandOff(AbstractSensor sensor){
		this.sensor=sensor;
		
		
	}
	public void execute() {
			
		sensor.off();

	}
	
	

}

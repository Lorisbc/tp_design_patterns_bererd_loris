package eu.telecomnancy.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface ICommand {
	
	void execute() throws SensorNotActivatedException;
	

}

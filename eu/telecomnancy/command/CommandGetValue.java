package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandGetValue implements ICommand {
	
	double value;
	
	private AbstractSensor sensor;
	public CommandGetValue(AbstractSensor sensor){
		this.sensor=sensor;
		
		
	}

	public void execute() throws SensorNotActivatedException {
		value=sensor.getValue();
	}
	
	public double get() throws SensorNotActivatedException{
		return value;
	}

}

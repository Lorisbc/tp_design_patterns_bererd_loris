package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;

public class CommandOn implements ICommand {

	private AbstractSensor sensor;
	public CommandOn(AbstractSensor sensor){
		this.sensor=sensor;
		
		
	}
	public void execute() {
			
		sensor.on();

	}

}

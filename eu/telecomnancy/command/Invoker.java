package eu.telecomnancy.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Invoker {
	
	private ICommand cmd;

	public void execute(ICommand cmd) throws SensorNotActivatedException {
		this.cmd=cmd;
		cmd.execute();
	}
}

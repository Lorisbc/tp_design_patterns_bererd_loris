package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandUpdate implements ICommand{
	
	private AbstractSensor sensor;
	public CommandUpdate(AbstractSensor sensor){
		this.sensor=sensor;
		
		
	}
	public void execute() throws SensorNotActivatedException {
			
		sensor.update();

	}

}

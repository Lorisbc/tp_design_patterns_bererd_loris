package eu.telecomnancy.factory;

import java.lang.reflect.Constructor;

import eu.telecomnancy.command.ICommand;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.sensor.StateSensor;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorFactory {
	private AbstractSensor sensor;
	
	public AbstractSensor create(String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		
		
		//On crée un objet Class
		Class cl = Class.forName(name);



		AbstractSensor sensor = (AbstractSensor) cl.newInstance();
		
		return sensor;
		
		

	}

}

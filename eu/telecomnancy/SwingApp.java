package eu.telecomnancy;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.factory.SensorFactory;
import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.sensor.StateSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {

		SensorFactory SensorFactory = new SensorFactory();

		// SensorFactory.create("class Name")
		// You can change the file app.properties, choose between: 
		// eu.telecomnancy.sensor.StateSensor , eu.telecomnancy.sensor.Proxy, eu.telecomnancy.sensor.Adapter , eu.telecomnancy.sensor.TemperatureSensor

		ReadPropertyFile rp=new ReadPropertyFile();
		Properties p=rp.readFile("/eu/telecomnancy/app.properties");
		for (String i: p.stringPropertyNames()) {

			new MainWindow(SensorFactory.create(p.getProperty(i)));
		}

	}
}

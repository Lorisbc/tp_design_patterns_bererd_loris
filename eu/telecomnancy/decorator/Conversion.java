package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Conversion extends AbstractDecorateur {
	
	private boolean degre=true;
	
	public Conversion(AbstractSensor sensor) {
		super(sensor);
		
	}

	public double convert() throws SensorNotActivatedException{
		
		double val=sensor.getValue();
		val=val*1.8+32;
		return val;
	}

	public boolean isDegre() {
		return degre;
	}

	public void setDegre(boolean degre) {
		this.degre = degre;
	}
	
	

}

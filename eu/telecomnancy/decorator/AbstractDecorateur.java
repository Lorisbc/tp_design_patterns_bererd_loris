package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class AbstractDecorateur extends AbstractSensor  {

	protected AbstractSensor sensor;
	
	public AbstractDecorateur(AbstractSensor sensor){
		this.sensor=sensor;
	}
	@Override
	public void on() {
		sensor.on();

	}

	@Override
	public void off() {
		sensor.off();

	}

	@Override
	public boolean getStatus() {
		
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();

	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}

}

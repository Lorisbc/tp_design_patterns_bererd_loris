package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Round extends AbstractDecorateur {

	public Round(AbstractSensor sensor) {
		super(sensor);
		
	}
	
public double round() throws SensorNotActivatedException{
		
		double val=sensor.getValue();
		val= Math.round(val);
		return val;
	}

}
